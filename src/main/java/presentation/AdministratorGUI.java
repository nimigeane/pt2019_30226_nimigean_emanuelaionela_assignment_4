package presentation;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;
import java.util.Map.Entry;

import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;

import business.BaseProduct;
import business.IRestaurantProcessing;
import business.MenuItem;
import business.Restaurant;

import javax.swing.JButton;

public class AdministratorGUI extends JFrame {

	private JFrame frame;
	private JTextField txtIdCompositeProduct;
	private JTextField txtIdProduct1;
	private JTextField txtName1;
	private JTextField txtPrice1;
	private JTextField txtIdProduct2;
	private JTextField txtName2;
	private JTextField txtPrice2;
	private JTextField txtIdProduct3;
	private JTextField txtName3;
	private JTextField txtPrice3;

	/**
	 * Launch the application.
	 */
	public static void NewScreen() {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					AdministratorGUI window = new AdministratorGUI();
					window.frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	public static JTable crTab(ArrayList<MenuItem> arr) {
	
		IRestaurantProcessing r=new Restaurant();
		Hashtable<Integer,ArrayList<MenuItem>> H=new Hashtable<Integer,ArrayList<MenuItem>>();
		H=((Restaurant) r).createHT(arr);
		
		 DefaultTableModel dtm = new DefaultTableModel();


		   for(Entry<Integer, ArrayList<MenuItem>> entry: H.entrySet()) {
		      dtm.addRow(new Object[] {entry.getKey(), entry.getValue()});
		    
		   }
		   JTable table = new JTable(dtm);
		   JOptionPane.showMessageDialog(null, new JScrollPane(table));
		
		return table;
	}

	/**
	 * Create the frame.
	 */
	public AdministratorGUI() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 450, 300);

		JPanel panel = new JPanel();

		frame.getContentPane().add(panel, BorderLayout.CENTER);
		panel.setLayout(null);

		JLabel lbl = new JLabel("Add Menu Item");
		lbl.setFont(new Font("Tahoma", Font.PLAIN, 20));
		lbl.setBounds(10, 11, 180, 25);
		panel.add(lbl);

		txtIdCompositeProduct = new JTextField();
		txtIdCompositeProduct.setText("idCompositeProduct");
		txtIdCompositeProduct.setBounds(10, 50, 120, 20);
		panel.add(txtIdCompositeProduct);
		txtIdCompositeProduct.setColumns(10);

		txtIdProduct1 = new JTextField();
		txtIdProduct1.setText("idProduct1");
		txtIdProduct1.setBounds(135, 50, 70, 20);
		panel.add(txtIdProduct1);
		txtIdProduct1.setColumns(10);

		txtName1 = new JTextField();
		txtName1.setText("name");
		txtName1.setBounds(210, 50, 70, 20);
		panel.add(txtName1);
		txtName1.setColumns(10);

		txtPrice1 = new JTextField();
		txtPrice1.setText("price");
		txtPrice1.setBounds(285, 50, 70, 20);
		panel.add(txtPrice1);
		txtPrice1.setColumns(10);

		txtIdProduct2 = new JTextField();
		txtIdProduct2.setText("idProduct2");
		txtIdProduct2.setBounds(135, 80, 70, 20);
		panel.add(txtIdProduct2);
		txtIdProduct2.setColumns(10);

		txtName2 = new JTextField();
		txtName2.setText("name");
		txtName2.setBounds(210, 80, 70, 20);
		panel.add(txtName2);
		txtName2.setColumns(10);

		txtPrice2 = new JTextField();
		txtPrice2.setText("price");
		txtPrice2.setBounds(285, 80, 70, 20);
		panel.add(txtPrice2);
		txtPrice2.setColumns(10);

		txtIdProduct3 = new JTextField();
		txtIdProduct3.setText("idProduct3");
		txtIdProduct3.setBounds(135, 110, 70, 20);
		panel.add(txtIdProduct3);
		txtIdProduct3.setColumns(10);

		txtName3 = new JTextField();
		txtName3.setText("name");
		txtName3.setBounds(210, 110, 70, 20);
		panel.add(txtName3);
		txtName3.setColumns(10);

		txtPrice3 = new JTextField();
		txtPrice3.setText("price");
		txtPrice3.setBounds(285, 110, 70, 20);
		panel.add(txtPrice3);
		txtPrice3.setColumns(10);

		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String txt = txtIdCompositeProduct.getText();
				String txt2 = txtIdProduct1.getText();
				String txt3 = txtName1.getText();
				String txt4 = txtPrice1.getText();
				String txt22 = txtIdProduct2.getText();
				String txt32 = txtName2.getText();
				String txt42 = txtPrice2.getText();
				String txt23 = txtIdProduct3.getText();
				String txt33 = txtName3.getText();
				String txt43 = txtPrice3.getText();
				int idcp = Integer.parseInt(txt);
				int idp1 = Integer.parseInt(txt2);
				int idp2 = Integer.parseInt(txt22);
				int idp3 = Integer.parseInt(txt23);
				double price1 = Double.parseDouble(txt4);
				double price2 = Double.parseDouble(txt42);
				double price3 = Double.parseDouble(txt43);
				try {
					IRestaurantProcessing r = new Restaurant();
					MenuItem m = new BaseProduct(idp1, txt3, price1);
					((Restaurant) r).createMenuItem(m);

					JTable table = crTab(((Restaurant) r).getMenu());

					Vector<Vector<Object>> data = new Vector<Vector<Object>>();
					for (MenuItem m1 : ((Restaurant) r).getMenu()) {
						Vector<Object> vector = new Vector<Object>();
						for (int columnIndex = 1; columnIndex <= 3; columnIndex++) {
							vector.add(m1);
						}
						data.add(vector);
					}

					JOptionPane.showMessageDialog(null, new JScrollPane(table));

				} catch (Exception ex) {
				}
			}
		});
		btnAdd.setBounds(1, 80, 90, 23);
		panel.add(btnAdd);

		JButton btnView = new JButton("View");
		btnView.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				IRestaurantProcessing r = new Restaurant();
				JTable table = crTab(((Restaurant) r).getMenu());

				Vector<Vector<Object>> data = new Vector<Vector<Object>>();
				for (MenuItem m : ((Restaurant) r).getMenu()) {
					Vector<Object> vector = new Vector<Object>();
					for (int columnIndex = 1; columnIndex <= 3; columnIndex++) {
						vector.add(m);
					}
					data.add(vector);
				}

				JOptionPane.showMessageDialog(null, new JScrollPane(table));

			}
		});
		btnView.setBounds(1, 170, 90, 23);
		panel.add(btnView);

		JButton btnBack = new JButton("Back");
		btnBack.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				frame.setVisible(false);
			}
		});
		btnBack.setBounds(1, 200, 89, 23);
		panel.add(btnBack);

		frame.addWindowListener(new WindowAdapter() {

			public void windowClosing(WindowEvent e) {
				Restaurant.update();
			}
		});
	}
}
