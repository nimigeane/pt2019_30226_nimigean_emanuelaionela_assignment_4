package business;

import java.util.Date;
import java.util.Hashtable;

public class Order {
	private int orderId;
	private int tableNo;
	private Date dateOrder;
	
	public int getOrderId() {
		return orderId;
	}

	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}

	public int getTableNo() {
		return tableNo;
	}

	public void setTableNo(int tableNo) {
		this.tableNo = tableNo;
	}

	public Date getDateOrder() {
		return dateOrder;
	}

	public void setDateOrder(Date dateOrder) {
		this.dateOrder = dateOrder;
	}

	public int hashCode() {
		return orderId;
		
	}
}
