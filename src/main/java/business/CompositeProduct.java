package business;

import java.util.ArrayList;


public class CompositeProduct implements MenuItem{
	private int idCompositeProduct;
	private ArrayList<MenuItem> compo = new ArrayList<MenuItem>();
	
	public int getIdCompositeProduct() {
		return idCompositeProduct;
	}

	public void setIdCompositeProduct(int idCompositeProduct) {
		this.idCompositeProduct = idCompositeProduct;
	}

	public ArrayList<MenuItem> getCompo() {
		return compo;
	}

	public void setCompo(ArrayList<MenuItem> compo) {
		this.compo = compo;
	}


	public CompositeProduct(int idCompositeProduct, ArrayList<MenuItem> men) {
		super();
		this.idCompositeProduct = idCompositeProduct;
		this.compo=men;
	}

	public double computePrice() {
		double s=0;
		for (MenuItem b : this.compo)
			s+=b.computePrice();
		return s;
	}
	
	public String toString() {
		String r = "Composite product no: "+this.idCompositeProduct+" ";
		for (MenuItem b : this.compo)
			r = r + b.toString()+"; ";
		return r;
	}

}

