package business;

public class BaseProduct implements MenuItem{
	private int idProduct;
	private String nameProduct;
	private double priceProduct;
	
	public BaseProduct(int idProduct,String nameProduct, double priceProduct) {
		super();
		this.idProduct=idProduct;
		this.nameProduct = nameProduct;
		this.priceProduct = priceProduct;
	}

	public int getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(int idProduct) {
		this.idProduct = idProduct;
	}

	public String getNameProduct() {
		return nameProduct;
	}

	public void setNameProduct(String nameProduct) {
		this.nameProduct = nameProduct;
	}

	public double getPriceProduct() {
		return priceProduct;
	}

	public void setPriceProduct(double priceProduct) {
		this.priceProduct = priceProduct;
	}

	public double computePrice() {
		return getPriceProduct();	
	}
	
	public String toString() {
		return "idProduct: "+this.getIdProduct()+",name: "+this.getNameProduct()+", price: "+this.getPriceProduct();
	}

	
/*	public static void main(String args[]) {
		BaseProduct pr=new BaseProduct("snitel pui",5.556);
		System.out.println(pr.getNameProduct());
		BaseProduct pr2=new BaseProduct("limonada",8);
		System.out.println(pr2.computePrice());
	}*/
}
