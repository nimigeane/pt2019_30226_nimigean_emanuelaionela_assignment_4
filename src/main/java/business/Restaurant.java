package business;

import java.awt.print.PrinterException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;
import java.util.Map.Entry;

import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import business.BaseProduct;
import data.RestaurantSerializator;

public class Restaurant implements IRestaurantProcessing{

	private MenuItem menuItem;
	private Order order;
	private static HashMap<Order,ArrayList<MenuItem>> hash;
	public static ArrayList<MenuItem> menu;
	private static ArrayList<Order> orderArrList;
	private static Hashtable<Integer, ArrayList<MenuItem>> hashtable;
	
	public Restaurant() {
		
	
		hash=new HashMap<>();
		
		menu = RestaurantSerializator.deSerialize();

		//if (menu == null) {
		//	menu = new ArrayList<MenuItem>();
		//}

		//menu=new ArrayList<MenuItem>();
		BaseProduct pr88=new BaseProduct(88,"din constructor",99);
		menu.add(pr88);
		
		hashtable=new Hashtable<Integer, ArrayList<MenuItem>>();
	}
	
	
	public static ArrayList<MenuItem> getMenu() {
		return menu;
	}


	public static void setMenu(ArrayList<MenuItem> menu) {
		Restaurant.menu = menu;
	}


	public void createMenuItem(MenuItem m) {
		menu.add(m);
		
	}

	public void deleteMenuItem(int id) {
		MenuItem mi=searchById(id);
		menu.remove(mi);
		
	}

	public void editMenuItem(int id, MenuItem m) {
		/*for(MenuItem x:Restaurant.menu) {
			if(x instanceof BaseProduct && ((BaseProduct) x).getIdProduct()==id)
				x=m;
			else if(x instanceof CompositeProduct && ((CompositeProduct) x).getIdCompositeProduct()==id)
				x=m;
		}*/
		deleteMenuItem(id);
		createMenuItem(m);
		
	}
	
	public MenuItem searchById(int id) {
		//MenuItem x=null;
		for(MenuItem x:Restaurant.menu) {
			if(x instanceof BaseProduct && ((BaseProduct) x).getIdProduct()==id)
				return x;
			else if(x instanceof CompositeProduct && ((CompositeProduct) x).getIdCompositeProduct()==id)
				return x;
		}
		return null;
	}
	
	public static Hashtable<Integer,ArrayList<MenuItem>> createHT(ArrayList<MenuItem> arr) {
		
		  Map map=new HashMap();
		   for(MenuItem z:menu) {
			   int in=0;
				if(z instanceof BaseProduct)
					in= ((BaseProduct) z).getIdProduct();
				else if(z instanceof CompositeProduct )
					in=((CompositeProduct) z).getIdCompositeProduct();
			   map.put(in, z);
		   }
		   hashtable.putAll(map);
		  // System.out.println(hashtable);
		  
		return hashtable;
		
	}

	public void createOrder() {
		// TODO Auto-generated method stub
		
	}

	public void computePriceOrder() {
		// TODO Auto-generated method stub
		
	}

	public void generateBill() {
		// TODO Auto-generated method stub
		
	}


	public static void update() {
		RestaurantSerializator.serialize(menu);
		
	}
	

}
