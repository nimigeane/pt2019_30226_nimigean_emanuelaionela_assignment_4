package data;
import business.*;


import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import business.MenuItem;

public class RestaurantSerializator {
	
	public static void serialize(ArrayList<MenuItem> e) {
		try {
			
	         FileOutputStream outputFile = new FileOutputStream("streamOutMenu.txt");
	         ObjectOutputStream outputObject = new ObjectOutputStream(outputFile);
	         outputObject.writeObject(e);
	         outputObject.close();
	         outputFile.close();
	         
	      } catch (IOException i) {
	         i.printStackTrace();
	      }
	}
	public static ArrayList<MenuItem> deSerialize() {
		try {
			
	         FileInputStream inputFile = new FileInputStream("streamOutMenu.txt");
	         ObjectInputStream inputObject = new ObjectInputStream(inputFile);
	         ArrayList<MenuItem> e = (ArrayList<MenuItem>) inputObject.readObject();
	         inputObject.close();
	         inputFile.close();
	         return e;
	         
	      } catch (IOException io) {
	         io.printStackTrace();
	        
	      } catch (ClassNotFoundException cnf) {
	         cnf.printStackTrace();
	        
	      }
		return null;
		
	}
	}

